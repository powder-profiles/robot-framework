"""Install the Robot Framework
"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# And Emulab extensions
import geni.rspec.emulab as emulab

INSTALL = "sudo /bin/bash /local/repository/install.sh"
IMAGE   = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD"


# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

pc.defineParameter("phystype",  "Optional physical node type",
                   portal.ParameterType.NODETYPE, "d430")

pc.defineParameter("usevm",  "Use a shared VM instead",
                   portal.ParameterType.BOOLEAN, True)

# Retrieve the values the user specifies during instantiation.
params = pc.bindParameters()

if params.usevm:
    node = request.XenVM("robot")
    node.exclusive = False
    node.ram = 8192
    node.cores = 2
    node.routable_control_ip = True
else:    
    node = request.RawPC("robot")
    node.hardware_type = params.phystype
    pass

node.disk_image = IMAGE
node.addService(pg.Execute(shell="bash", command=INSTALL))
node.startVNC()

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
