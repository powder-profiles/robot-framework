#!/bin/sh

SCRIPTNAME=$0

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi
HOMEDIR="/users/$USER"
SSHDIR="$HOMEDIR/.ssh"

# Geni key to create an SSH key pair.
if [ ! -e $SSHDIR/id_rsa ]; then
    geni-get key > $SSHDIR/id_rsa
    if [ $? -ne 0 ]; then
	echo "ERROR: geni-get key failed!"
	exit 1
    fi
    chmod 600 $SSHDIR/id_rsa
fi

if [ ! -e $SSHDIR/id_rsa.pub ]; then
    ssh-keygen -y -f $SSHDIR/id_rsa > $SSHDIR/id_rsa.pub
    if [ $? -ne 0 ]; then
	echo "ERROR: ssh-keygen failed!"
	exit 1
    fi
    cat $SSHDIR/id_rsa.pub >> $SSHDIR/authorized_keys 
fi

exit 0;
