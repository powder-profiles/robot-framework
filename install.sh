#!/bin/sh

SCRIPTNAME=$0

#
# Might not be on the local cluster, so need to use the urn to
# see who the actual creator is.
#
GENIUSER=`geni-get user_urn | awk -F+ '{print $4}'`
if [ $? -ne 0 ]; then
    echo "ERROR: could not run geni-get user_urn!"
    exit 1
fi
if [ $USER != $GENIUSER ]; then
    sudo -u $GENIUSER $SCRIPTNAME
    exit $?
fi
HOMEDIR="/users/$USER"

sudo apt-get update
if [ $? -ne 0 ]; then
    echo 'apt-get update failed'
    exit 1
fi

sudo apt-get install --no-install-recommends libjson-perl -y
if [ $? -ne 0 ]; then
    echo 'apt-get install libjson-perl failed'
    exit 1
fi

sudo apt-get install python3-pip -y
if [ $? -ne 0 ]; then
    echo 'apt-get install pip failed'
    exit 1
fi

sudo apt-get install --no-install-recommends python3-paramiko -y
if [ $? -ne 0 ]; then
    echo 'apt-get install python3-paramiko failed'
    exit 1
fi

sudo pip install --upgrade robotframework robotframework-sshlibrary
if [ $? -ne 0 ]; then
    echo 'pip install robot and friends failed'
    exit 1
fi

#
# Setup ssl certificate for talking to the portal RPC server
#
/local/repository/setup-ssl.sh
if [ $? -ne 0 ]; then
    echo '/local/repository/setup-ssl.sh failed'
    exit 1
fi

exit 0
