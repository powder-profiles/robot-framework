#!/usr/bin/perl -w

use strict;
use Carp;
use English;
use Data::Dumper;
use Getopt::Std;
use JSON;

# Drag in path stuff so we can find emulab stuff.
BEGIN { require "/etc/emulab/paths.pm"; import emulabpaths; }

#
# For use in stackstorm, we need to get the key of the system
# under test experiment so we and ssh private key to log in.
#
sub usage()
{
    print STDOUT "Usage: stackstorm-setup [-d] [-k keyname] pid,eid\n";
    exit(-1);
}
my $optlist     = "dk:";
my $debug       = 0;
my $keyname     = "sut_rsa";
my $HOME        = $ENV{"HOME"};
my $SSHDIR      = "$HOME/.ssh";

#
# Turn off line buffering on output
#
$| = 1;

# Protos
sub fatal($);

#
# Parse command arguments.
#
my %options = ();
if (! getopts($optlist, \%options)) {
    usage();
}
if (defined($options{"d"})) {
    $debug = 1;
}
if (defined($options{"k"})) {
    $keyname = $options{"k"};
}
usage()
    if (@ARGV != 1);
my $eid = $ARGV[0];

# Grab the status blob.
my $json = `experimentStatus -j -k $eid`;
if ($?) {
    fatal("Could not get experiment status for $eid");
}
if ($debug > 1) {
    print $json;
}
my $status = eval { decode_json($json); };
if ($@) {
    fatal("Could not decode json data: $@");
}
if ($debug) {
    print $status->{'certificate'};
}
if (open(KEY, "> $SSHDIR/$keyname")) {
    my @lines = split("\n", $status->{'certificate'});
    while (@lines) {
	my $line = shift(@lines);
	next
	    if ($line !~ /^-----BEGIN RSA/);
	print KEY $line . "\n";
	while (@lines) {
	    my $line = shift(@lines);
	    print KEY $line . "\n";
	    last
		if ($line =~ /^-----END RSA/);
	}
	last;
    }
    close(KEY);
    system("/bin/chmod 600 $SSHDIR/$keyname");
    system("ssh-keygen -y -f $SSHDIR/$keyname > $SSHDIR/${keyname}.pub");
    if ($?) {
	fatal("Could not generate pub key from private key");
    }
}
else {
    fatal("Could not open $keyname for writing");
}
exit(0);

sub fatal($)
{
    my ($mesg) = $_[0];

    die("*** $0:\n".
	"    $mesg\n");
}
